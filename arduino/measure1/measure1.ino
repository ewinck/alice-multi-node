long previousMillis = 0;      
long interval = 20000; 

int analogPins[] = {0,1,2,3,4,5,6,7,8,9};
int readings[]   = {0,0,0,0,0,0,0,0,0,0};
int outVal[]     = {0,0,0,0,0,0,0,0,0,0};

int threshhold = 600;
int offsetTime = 0;

int incomingByte = 0; 

void setup() {                              
  Serial.begin(9600); 
  pinMode(analogPins[0],INPUT);
  pinMode(analogPins[1],INPUT);
  pinMode(analogPins[2],INPUT);
  pinMode(analogPins[3],INPUT);
  pinMode(analogPins[4],INPUT);
  pinMode(analogPins[5],INPUT);
  pinMode(analogPins[6],INPUT);
  pinMode(analogPins[7],INPUT);
  pinMode(analogPins[8],INPUT);
  pinMode(analogPins[9],INPUT);
  
  pinMode(13,OUTPUT);

}



void loop() { 
	
	if (Serial.available()) {
		incomingByte = Serial.read();
		
		//Serial.println(incomingByte, DEC);
		
		if(incomingByte == 97) {
			resetProbe();
		}
	}
    
	
	readValue(0);
	readValue(1);
	readValue(2);
	readValue(3);
	
	/*
	unsigned long currentMillis = millis();
 
	if(currentMillis - previousMillis > interval) {
	    previousMillis = currentMillis; 
	   
    }
    */
		
}

void readValue(int node) {
	readings[node] = analogRead(analogPins[node]);
	if(readings[node] > threshhold) {
		getFirst();
		if(outVal[node] == 0 && offsetTime != 0) {
			outVal[node] = millis() - offsetTime;
			Serial.print(node+1);
			Serial.print(":");
			Serial.println(outVal[node]);
		}
	}  
}


void getFirst() {
	if(offsetTime==0) {
		offsetTime = millis();
	}	
}


void resetProbe() {
	offsetTime = 0;
	incomingByte = 0;
	readings[0] = 0;
	readings[1] = 0;
	readings[2] = 0;
	readings[3] = 0;
	readings[4] = 0;
	readings[5] = 0;
	readings[6] = 0;
	readings[7] = 0;
	readings[8] = 0;
	readings[9] = 0;
	
	outVal[0] = 0;
	outVal[1] = 0;
	outVal[2] = 0;
	outVal[3] = 0;
	outVal[4] = 0;
	outVal[5] = 0;
	outVal[6] = 0;
	outVal[7] = 0;
	outVal[8] = 0;
	outVal[9] = 0;
}



void printAll() {
	int i;
	for (i = 0; i < 4; i++) {
	  Serial.print(outVal[i]);
	  Serial.print(',');
	}
	Serial.println(" ");
}
