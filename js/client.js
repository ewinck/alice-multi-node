var counter = 0;
var socket;
var agentInfo;
var clientID;
var isPlaying = false;
var delayPlay = 0;
var audioReady = false;
var clientEnabled = false;
var connectionAddress = 'http://192.168.0.150:9000/';
var audioExt = '';
var audioName = 'superman2'; //kalimba,drum, noangel
var syncPacket;
/////////////////////////////////////////////////////////
//INIT //////////////////////////////////////////////////
/////////////////////////////////////////////////////////



$( window ).resize(function() {
  positionDivs();
});


function positionDivs() {
	alignResizeCenterDiv("#bgContent","#contentWrapper",false);
	alignResizeCenterDiv("#bgSuper","#contentWrapper",false);
	
	alignCenterDiv("#contentDiv","#contentWrapper");
}

$(document).ready(function() {
	
	positionDivs();
	
	var parser = new UAParser();
	var result = parser.getResult();
	//console.log(result.browser);        // {name: "Chromium", major: "15", version: "15.0.874.106"}
	//console.log(result.device);         // {model: undefined, type: undefined, vendor: undefined}
	//console.log(result.os);             // {name: "Ubuntu", version: "11.10"}
	//console.log(result.os.version);     // "11.10"
	//console.log(result.engine.name);    // "WebKit"
	
	$('#label').html('carregando...');
	
	$('#tituloBut').hide();
	
	if (result.browser.name == "Firefox") {
		audioExt = '.ogg';
	} else {
		audioExt = '.m4a';
	}
	var audioFile = 'audio/' + audioName + audioExt;
	var sound = new Howl({
		urls: [audioFile],
		autoplay: false,
		loop: false,
		volume: 1,
		onend: function() {},
		onload: function() {
			$('#tituloBut').html('start!');
			$('#tituloBut').show();
			$('#label').hide();
			audioReady = true;
		}
	});
	/////////////////////////////////////////////////////////
	//socket ////////////////////////////////////////////////
	/////////////////////////////////////////////////////////
	socket = io.connect(connectionAddress);
	/////////////////////////////////////////////////////////
	socket.on('connect', function() {
		clientID = socket.socket.sessionid;
		//$('#label').html(clientID);
		
		socket.emit('register client', {
			id: clientID,
			agent: navigator.userAgent
		});
	});
	/////////////////////////////////////////////////////////
	socket.on('message', function(msg) {});
	/////////////////////////////////////////////////////////
	socket.on('start time test', function(data) {
		data.clientTimeSend = now().getTime();
		data.clientId = clientID;
		socket.emit('time test', data);
	});
	/////////////////////////////////////////////////////////
	socket.on('finish time test', function(data) {
		data.latencyAverage = getAverageFromNumArr(filterSamples(data.latencyArray));
		//$('#label4').html("<pre>" + JSON.stringify(data, undefined, 2) + "</pre>");
		//$('#label5').html("<pre>" + JSON.stringify(filterSamples(data.latencyArray), undefined, 2) + "</pre>");
		socket.emit('update latency server', data);
	});
	/////////////////////////////////////////////////////////
	socket.on('start play', function(data) {
		delayPlay = data;
		if (audioReady && clientEnabled && (isPlaying == false)) {
			startPlaySound();
		}
		//animloop();

		function animloop() {
			$('#label2').html('anim');
			requestAnimFrame(animloop);
		}
	});
	/////////////////////////////////////////////////////////
	socket.on('stop', function(data) {
		//console.log("stop");
		if (clientEnabled && (isPlaying == true)) {
			$('#tituloBut').html('aguarde...');
			$('#bgSuper').hide();
			isPlaying = false;
			sound.stop();
		}
	});
	/////////////////////////////////////////////////////////
	socket.on('reset', function(data) {
		location.reload();
	});
	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////
	$(".button").on('touchstart click', function(e) {
		if (audioReady && (clientEnabled == false) && (isPlaying == false)) {
			$('#tituloBut').html('aguarde...');
			//console.log("click but");
			sound.play();
			sound.pause();
			socket.emit('client enabled', {
				id: clientID
			});
			clientEnabled = true
		}
	});

	function startPlaySound() {
		console.log("play audio");
		var delayDevice = 0;
		isPlaying = true;
		if (result.device.model == 'iPhone') {
			delayDevice = 0;
		} else if (result.device.model == 'iPad') {
			delayDevice = 0;
		} else if ((result.browser.name == 'Chrome') && (result.device.model == '')) {
			delayDevice = 0;
		} else if ((result.browser.name == 'Chrome') && (result.device.model == 'Nexus 4')) {
			delayDevice = 0;
		} else if (result.browser.name == 'Safari') {
			delayDevice = 0;
		} else if (result.browser.name == 'Firefox') {
			delayDevice = -10;
		}
		
		var calculatedDelay = 500 - delayPlay - delayDevice;
		var timer = window.requestTimeout(playCommand, calculatedDelay);
		
		/*
		setTimeout(function() {
			sound.play();
			$('#tituloBut').html('play!');
		}, 500 - delayPlay - delayDevice);
		
		*/
	}
	
	function playCommand() {
		sound.play();
		$('#tituloBut').html('tocando!');
		$('#bgSuper').show();
	}
});


var filterSamples = function(samples) {
		var list = [],
			i, latency, len = samples.length,
			sd = getStandardDeviation(samples),
			median = calcMedian(samples);
		for (i = 0; i < len; i++) {
			latency = samples[i];
			//console.log(latency + ' - ' + median + ' - ' + sd);
			//console.log(latency +' > '+ (median-sd) + ' = ' + (latency > (median - sd)));
			//console.log(latency +' < '+ (median+sd) + ' = ' + (latency < (median + sd)));
			//console.log("");
			if ((latency > (median - sd)) && (latency < (median + sd))) {
				list.push(latency);
			}
		}
		return list;
	}
	
	
var calcMedian = function(values) {
		var half = Math.floor(values.length / 2);
		if (values.length % 2) {
			return values[half];
		} else {
			return (values[half - 1] + values[half]) / 2.0;
		}
	};
	
	
// http://bateru.com/news/2011/03/javascript-standard-deviation-variance-average-functions/
var isArray = function(obj) {
		return Object.prototype.toString.call(obj) === "[object Array]";
	},
	getNumWithSetDec = function(num, numOfDec) {
		var pow10s = Math.pow(10, numOfDec || 0);
		return (numOfDec) ? Math.round(pow10s * num) / pow10s : num;
	},
	getAverageFromNumArr = function(numArr, numOfDec) {
		if (!isArray(numArr) || numArr.length == 0) {
			return 0;
		}
		var i = numArr.length,
			sum = 0;
		while (i--) {
			sum += numArr[i];
		}
		return getNumWithSetDec((sum / numArr.length), numOfDec);
	},
	getVariance = function(numArr, numOfDec) {
		if (!isArray(numArr)) {
			return false;
		}
		var avg = getAverageFromNumArr(numArr, numOfDec),
			i = numArr.length,
			v = 0;
		while (i--) {
			v += Math.pow((numArr[i] - avg), 2);
		}
		v /= numArr.length;
		return getNumWithSetDec(v, numOfDec);
	},
	getStandardDeviation = function(numArr, numOfDec) {
		if (!isArray(numArr)) {
			return false;
		}
		var stdDev = Math.sqrt(getVariance(numArr, numOfDec));
		return getNumWithSetDec(stdDev, numOfDec);
	};
	
	
/*
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());
*/
function formatDateObj(dateObj) {
	var hours = dateObj.getHours();
	var minutes = dateObj.getMinutes();
	var seconds = dateObj.getSeconds();
	var millis = dateObj.getMilliseconds();
	if (hours < 10) {
		hours = "0" + hours;
	}
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	var time = hours + ':' + minutes + ':' + seconds + ":" + millis;
	return time;
}


function debugVar(texto) {
	$('#label').html(texto);
	console.log(texto);
}


function now() {
	return new Date();
}


window.requestAnimFrame = (function() {
	return  window.requestAnimationFrame       || 
			window.webkitRequestAnimationFrame || 
			window.mozRequestAnimationFrame    || 
			window.oRequestAnimationFrame      || 
			window.msRequestAnimationFrame     || 
			function(/* function */ callback, /* DOMElement */ element){
				window.setTimeout(callback, 1000 / 60);
			};
})();

/**
 * Behaves the same as setInterval except uses requestAnimationFrame() where possible for better performance
 * @param {function} fn The callback function
 * @param {int} delay The delay in milliseconds
 */
window.requestInterval = function(fn, delay) {
	if( !window.requestAnimationFrame       && 
		!window.webkitRequestAnimationFrame && 
		!(window.mozRequestAnimationFrame && window.mozCancelRequestAnimationFrame) && // Firefox 5 ships without cancel support
		!window.oRequestAnimationFrame      && 
		!window.msRequestAnimationFrame)
			return window.setInterval(fn, delay);
			
	var start = new Date().getTime(),
		handle = new Object();
		
	function loop() {
		var current = new Date().getTime(),
			delta = current - start;
			
		if(delta >= delay) {
			fn.call();
			start = new Date().getTime();
		}
 
		handle.value = requestAnimFrame(loop);
	};
	
	handle.value = requestAnimFrame(loop);
	return handle;
}
 
/**
 * Behaves the same as clearInterval except uses cancelRequestAnimationFrame() where possible for better performance
 * @param {int|object} fn The callback function
 */
    window.clearRequestInterval = function(handle) {
    window.cancelAnimationFrame ? window.cancelAnimationFrame(handle.value) :
    window.webkitCancelAnimationFrame ? window.webkitCancelAnimationFrame(handle.value) :
    window.webkitCancelRequestAnimationFrame ? window.webkitCancelRequestAnimationFrame(handle.value) : /* Support for legacy API */
    window.mozCancelRequestAnimationFrame ? window.mozCancelRequestAnimationFrame(handle.value) :
    window.oCancelRequestAnimationFrame	? window.oCancelRequestAnimationFrame(handle.value) :
    window.msCancelRequestAnimationFrame ? window.msCancelRequestAnimationFrame(handle.value) :
    clearInterval(handle);
};









function alignResizeCenterDiv(divIN,divOUT,noOutside) {
	
	var divOUTHeight = $(divOUT).height();
	var divOUTWidth = $(divOUT).width();
	var ratioOUT = divOUTWidth/divOUTHeight;
	
	var divINHeight = $(divIN).height();
	var divINWidth = $(divIN).width();
	var ratioIN = divINWidth/divINHeight;
	
	if(noOutside) {
		if(ratioOUT>=ratioIN) {
			$(divIN).css('top','0px');
			$(divIN).css('left',(divOUTWidth/2)-((divOUTHeight*ratioIN)/2)+'px');
			$(divIN).css('width',(divOUTHeight*ratioIN)+'px');
			$(divIN).css('height',divOUTHeight+'px');
		} else {
			$(divIN).css('left',(divOUTWidth/2)-((divOUTHeight*ratioIN)/2)+'px');
			$(divIN).css('top','0px');
			$(divIN).css('height',divOUTHeight+'px');
			$(divIN).css('width',(divOUTHeight*ratioIN)+'px');
		}
	} else {
		if(ratioOUT>=ratioIN) {
			$(divIN).css('top',(divOUTHeight/2)-((divOUTWidth/ratioIN)/2)+'px');
			$(divIN).css('left','0px');
			$(divIN).css('width',divOUTWidth+'px');
			$(divIN).css('height',(divOUTWidth/ratioIN)+'px');
		} else {
			$(divIN).css('left',(divOUTWidth/2)-((divOUTHeight*ratioIN)/2)+'px');
			$(divIN).css('top','0px');
			$(divIN).css('height',divOUTHeight+'px');
			$(divIN).css('width',(divOUTHeight*ratioIN)+'px');
		}
	}
	
}


function alignCenterDiv(divIN,divOUT) {
	
	var divOUTHeight = $(divOUT).height();
	var divOUTWidth = $(divOUT).width();
	var ratioOUT = divOUTWidth/divOUTHeight;
	
	var divINHeight = $(divIN).height();
	var divINWidth = $(divIN).width();
	var ratioIN = divINWidth/divINHeight;
	

	$(divIN).css('top',(divOUTHeight/2)-((divINHeight)/2)+'px');
	$(divIN).css('left',(divOUTWidth/2)-((divINWidth)/2)+'px');
	
}



/**
 * Behaves the same as setTimeout except uses requestAnimationFrame() where possible for better performance
 * @param {function} fn The callback function
 * @param {int} delay The delay in milliseconds
 */
 
window.requestTimeout = function(fn, delay) {
	if( !window.requestAnimationFrame      	&& 
		!window.webkitRequestAnimationFrame && 
		!(window.mozRequestAnimationFrame && window.mozCancelRequestAnimationFrame) && // Firefox 5 ships without cancel support
		!window.oRequestAnimationFrame      && 
		!window.msRequestAnimationFrame)
			return window.setTimeout(fn, delay);
			
	var start = new Date().getTime(),
		handle = new Object();
		
	function loop(){
		var current = new Date().getTime(),
			delta = current - start;
			
		delta >= delay ? fn.call() : handle.value = requestAnimFrame(loop);
	};
	
	handle.value = requestAnimFrame(loop);
	return handle;
};
 
/**
 * Behaves the same as clearTimeout except uses cancelRequestAnimationFrame() where possible for better performance
 * @param {int|object} fn The callback function
 */
window.clearRequestTimeout = function(handle) {
    window.cancelAnimationFrame ? window.cancelAnimationFrame(handle.value) :
    window.webkitCancelAnimationFrame ? window.webkitCancelAnimationFrame(handle.value) :
    window.webkitCancelRequestAnimationFrame ? window.webkitCancelRequestAnimationFrame(handle.value) : /* Support for legacy API */
    window.mozCancelRequestAnimationFrame ? window.mozCancelRequestAnimationFrame(handle.value) :
    window.oCancelRequestAnimationFrame	? window.oCancelRequestAnimationFrame(handle.value) :
    window.msCancelRequestAnimationFrame ? window.msCancelRequestAnimationFrame(handle.value) :
    clearTimeout(handle);
};