var clientsArray = new Object();
var roomName = 'devices';
var latencyBurstTimes = 10;
var countUsersReady = 0;

var serverTime = 0;

var syncPacket = {
      clientTimeSend: 0,
      serverTimeSend: 0,  
      clientTimeReturn: 0,
      serverTimeReturn: 0,
      latencyArray: [],
      latencyAverage: 0
}



///////////////////////////////////////////////////////////////////////////

/*
var mysql      = require('mysql');

var connection = mysql.createConnection({
  user     : 'root',
  password : 'root',
  debug	   : false,
  database    : 'figwal',
  socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock'
});

connection.connect();

connection.query('SELECT * from noticias', function(err, rows, fields) {
  if (err) throw err;

  console.log(rows[0].titulo_pt);
});

connection.end();
*/

///////////////////////////////////////////////////////////////////////////

/*
var com = require("serialport");

var serialPort = new com.SerialPort("/dev/tty.usbserial-A600eD1X", {
    baudrate: 9600,
    parser: com.parsers.readline('\r\n')
});

serialPort.on('open',function() {
  console.log('Port open');
});

serialPort.on('data', function(data) {
  console.log(data);
});
*/


///////////////////////////////////////////////////////////////////////////






var io = require('socket.io').listen(9000);
io.set('log level', 1);

io.sockets.on('connection', function(socket) {
	/////////////////////////////////////////////////////////////////////////////
	socket.on('register client', function(data) {
		socket.join(roomName);
		addClient(data,io.sockets.sockets[data.id]);
	});
	/////////////////////////////////////////////////////////////////////////////
	socket.on('play clients', function(data) {
		serverTime = now().getTime();
		syncPacket.serverTimeSend = serverTime;
		countUsersReady = 0;
		socket.broadcast.to(roomName).emit('start time test', syncPacket);
	});
	/////////////////////////////////////////////////////////////////////////////
	socket.on('time test', function(data) {
		data.serverTimeReturn = now().getTime();
		
		var tempLatency = (data.serverTimeReturn - data.serverTimeSend)/2;
		data.latencyArray.push(tempLatency);
		
		if(data.latencyArray.length < latencyBurstTimes) {
			data.serverTimeSend = data.serverTimeReturn;
			socket.emit('start time test', data);
		} else {
			socket.emit('finish time test', data);
		}
		
	});
	/////////////////////////////////////////////////////////////////////////////
	socket.on('update latency server', function(data) {
		clientsArray[data.clientId].latencyAverage = Math.round(data.latencyAverage);
		
		countUsersReady++;
		
		if(countUsersReady == countConnectedUsers()) {
				// depois que todos os clientes foram medidos
				var maxDelay = 0;
				
				for(var key in clientsArray) {
					var delayClient = clientsArray[key].latencyAverage;
					if(delayClient > maxDelay) {
						maxDelay = delayClient;
					}
				}
				
				//console.log("final measuring delays > " + maxDelay);
				
				for(var key in clientsArray) {
					var sockUser = io.sockets.sockets[key];
					var delayClient = maxDelay - clientsArray[key].latencyAverage;
					//console.log("delayClient: " + key + " - " + delayClient);
					sockUser.emit('start play', delayClient);
				}
				
				updateDashboard();

		}
		
	});	
	/////////////////////////////////////////////////////////////////////////////
	socket.on('control connect', function(data) {
		socket.emit('update dashboard', clientsArray);
	});
	/////////////////////////////////////////////////////////////////////////////	
	socket.on('client enabled', function(data) {
		clientsArray[data.id].enabled = true;
		updateDashboard();
	});
	/////////////////////////////////////////////////////////////////////////////
	socket.on('stop clients', function(data) {
		//serialPort.write('a');
		socket.broadcast.to(roomName).emit('stop', {
			my: '1'
		});
	});
	/////////////////////////////////////////////////////////////////////////////
	socket.on('reset clients', function(data) {
		socket.broadcast.to(roomName).emit('reset', {
			my: '1'
		});
	});
	/////////////////////////////////////////////////////////////////////////////
	socket.on('message', function(msg) {
		
	});
	/////////////////////////////////////////////////////////////////////////////
	socket.on('disconnect', function() {
		setTimeout(function() {
			removeClient(socket.id);	
		}, 50);
		
	});
	
	/////////////////////////////////////////////////////////////////////////////
	
	function addClient(data,socketObj) {
		
		clientsArray[data.id] = {
			agent:data.agent,
			enabled:false
			//countTest:0,
			//countTestArray:new Array()
		};	
		
		updateDashboard();	
	}
	
	function countConnectedUsers() {
		var connectedUsers = 0;
		for(var key in clientsArray)
			connectedUsers++;
		
		return connectedUsers;
	}
	
	function removeClient(data) {
		delete clientsArray[data];
		updateDashboard();
		//printArray();
	}
	
	
	function printArray() {
		console.log("                   ");
		console.log(clientsArray);
		console.log("                   ");
		
	}
	
	function now() {
		return new Date();
	}
	
	function updateDashboard() {
		socket.broadcast.emit('update dashboard', clientsArray);
		printArray();
	}
	
	
	
});



