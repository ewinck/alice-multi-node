var delayPing = 100;
var booleanBut = false;
var counter = 0;

$(document).ready(function() {
  	debugVar('ready');
  	  	
  	$(".button").click(function() {
  		
  		$('#tituloBut').html('listening...');
  		
  		var client = new BinaryClient('ws://localhost:9000');
  		
		client.on('stream', function(stream, meta){   
			
			client.send('clientListening');
			 
		    stream.on('data', function(data){
		      debugVar(data);
		      if(data=='startPlay') {
			      $('#bg_audio').trigger("play");
		      }
		    });
		});
	  	
  	});
  	
  	document.getElementById('bg_audio').addEventListener('play', function() {
  		debugVar('playing');
  	});
  	document.getElementById('bg_audio').addEventListener('pause', function() {
  		debugVar('pause');
  	});
  	document.getElementById('bg_audio').addEventListener('progress', function() {
  		//debugVar('downloading');
  	});

  	/*
  	function playAudio() {
  		setTimeout(function() {
  			debugVar('start');
  			$('#bg_audio').trigger("play");
  		}, 5000);
  	}
  	
  	function startListenening() {
  		
	  		setTimeout(function() {
	  			if(booleanBut) {
	  				debugVar('ping');
	  				counter++;
	  				$('#tituloBut').html(counter + ' listening...');
	  				startListenening();
  				}
  			}, delayPing);
  		
	  	
  	}
  	*/

  	function debugVar(texto) {
  		$('#label').html(texto);
  		console.log(texto);
  	}
  });