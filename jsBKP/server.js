var homeDir = '/Users/ericwinck/Documents/WORK/REPOSITORIOS/alice-multi-node';
var binaryJsDir = '/Users/ericwinck/Documents/WORK/REPOSITORIOS/alice-multi-node/js/node_modules/binaryjs'

var BinaryServer = require(binaryJsDir).BinaryServer;

var fs = require('fs');
// Start Binary.js server
var server = BinaryServer({
	port: 9000
});

// Wait for new user connections
server.on('connection', function(client) {
	client.send('connected listening');
	//console.log('connect');
	
	client.on('stream', function(stream, meta){
		stream.on('data', function(data) {
			//console.log('comando: ' + data);
			if (data == 'startServer') {
				//client.send('server control started');
				console.log('server control started');
				
				for (var id in server.clients) {
					if (server.clients.hasOwnProperty(id)) {
						var otherClient = server.clients[id];
						//console.log(otherClient);
						if (otherClient != client) {
							var send = otherClient.send('startPlay');
						}
					}
				}
				
			}
			
			if (data == 'clientListening') {
				console.log('client Listening');
			}
		});
	});
	
});
