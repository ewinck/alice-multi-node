var homeDir = '/Users/ericwinck/Documents/WORK/REPOSITORIOS/alice-multi-node';
var binaryJsDir = '/usr/local/bin/node_modules/binaryjs'


var BinaryServer = require(binaryJsDir).BinaryServer;
var fs = require('fs');

// Start Binary.js server
var server = BinaryServer({port: 9000});
// Wait for new user connections
server.on('connection', function(client){
  // Stream a flower as a hello!
  var file = fs.createReadStream(homeDir + '/audio/1.mp3');
  client.send(file); 
});